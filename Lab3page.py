'''!@file               Lab3page.py
    @brief              Creates the main documentation page for Lab 3.
    @details            This file serves the sole purpose of generating html
                        documentation for Lab 3.

    @page page3         Lab 3 Documentation/Deliverables

    @section sec_intro2 Introduction
                        ME 305 Lab 3 relies on the use of 8 files that work
                        in tandem to read, zero, and take data from one or two 
                        quadrature encoders through a serial connection with
                        the Nucleo board. This program can also interface with 
                        1 or 2 motors, setting their duty cycles and recording
                        their position, change in position, and velocity.
                        
    @section sec_files3 Files in Lab 3
                        Considering a file hierarchy consisting of 3 layers, 
                        main.py and shares.py make up the high level layer. 
                        This layer instantiates objects and runs tasks.
                        The middle layer is made up our our three tasks:
                        taskUser.py, taskEncoder.py, and taskMotor.py which run 
                        sequentially, each with a period of 0.01 [s].
                        The bottom layer is the driver layer which consists 
                        of the drivers: encoder.py, motor.py, and DRV8847.py.
    
    @section sec_src3   Lab 3 Source Code
                        The source code for Lab 3 can be found in my repository 
                        at: https://bitbucket.org/jakelesher/me-305-labs
    
    @section sec_plot3  Lab 3 Plot
                        Here is a plot of the output from this program after 
                        pressing 'G' in the user interface to gather data for
                        30 seconds.
                        
                        \htmlonly
                        <a href="https://ibb.co/3zrb831"><img src="https://i.ibb.co/HdFyMS7/Encoder-Position-Plot.png" alt="Encoder-Position-Plot" border="0" /></a>
                        \endhtmlonly
                        
    @section sec_duty3  Lab 3 Velocity vs. Duty Cycle Plot
                        The plot below shows experimental data points as blue
                        dots, while the trendline represents the theoretical 
                        comparison of velocity and duty cycle. It can be seen 
                        that in the real world, a "dead zone" exists between the
                        duty cycles of -8% and 8%, in which the friction in the 
                        motor prevents movement, regardless of current/voltage.
    
                        \htmlonly
                        <a href="https://ibb.co/5nS6sfs"><img src="https://i.ibb.co/Yy13fqf/Velocity-Vs-Duty.png" alt="Velocity-Vs-Duty" border="0"></a>
                        \endhtmlonly
                        
    @section sec_fsm3   Lab 3 Task and Transition Diagrams
                        Here are the task and transition diagrams for the
                        finite-state machine used in this program.
                        
                        \htmlonly
                        <a href="https://ibb.co/QmFdjSx"><img src="https://i.ibb.co/ryGsf8X/Page1-1.png" alt="Page1-1" border="0"></a>
                        <a href="https://ibb.co/Gcv0Dgw"><img src="https://i.ibb.co/vZxHW25/Page2-2.png" alt="Page2-2" border="0"></a>
                        <a href="https://ibb.co/QDyhFxF"><img src="https://i.ibb.co/Hh5RD9D/Page3-1.png" alt="Page3-1" border="0"></a>
                        <a href="https://ibb.co/wSyskLL"><img src="https://i.ibb.co/T4cLfTT/Page4-1.png" alt="Page4-1" border="0"></a>
                        \endhtmlonly


    @author             Jake Lesher
    @author             Daniel Xu
    
    @copyright          License Info

    @date               February 16, 2022
'''