'''!
    @file       taskController.py
    
    @brief      The task that interfaces with the Closed Loop class.
    
    @details    This task's main function is to provide the ClosedLoop driver
                with the user-selected gain and Vref values.
    
                The source code can be found at my repository under "Lab 4":
                https://bitbucket.org/jakelesher/me-305-labs
                
    @author     Jake Lesher
    @author     Daniel Xu
    @date       02/23/2022
'''

from time import ticks_us, ticks_add, ticks_diff
import pyb  
import micropython, motor, DRV8847, shares, ClosedLoop

# Defining states

# Initialization state
S0_INIT = micropython.const(0)
# Set duty cycle state
S1_SET = micropython.const(1)
# Clear the fault condition state
S2_ACTIVE = micropython.const(2)


def taskControllerFcn(taskName, period, clFlag, Velocity, Vref, Duty1,Kp,Ki,Kd,Data,Duty2):
    '''!@brief      This function interacts with the ClosedLoop driver, sending 
                    a duty cycle based on the calculated error.
        @details    This function calls upon the driver to set the duty cycle
                    percentage for motor 1.
        @param      taskName is the name associated the with taskController in 
                    main.py. 
        @param      period sets the rate at which taskController is to run.
        @param      clFlag is the shared boolean that toggles closed-loop control.
        @param      Velocity is the share of the velocity value with 
                    taskEncoder.py
        @param      Vref is the share of the velocity requested by the user in
                    taskUser.
        @param      Duty1 is the share that contains the duty cycle percentage
                    for motor 1.
        @param      Kp is the share of the user-requested proportional gain.
        @param      Ki is the share of the user-requested integral gain.
    '''
    
    # State 0 is used only for initialization, so it will not exist within 
    # the while loop.
    state = S0_INIT

    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    prev_time = start_time
    ClosedLoopControl_1 = ClosedLoop.ClosedLoop()
    ClosedLoopControl_2 = ClosedLoop.ClosedLoop()
    state = S1_SET
 
    while True:
        current_time = ticks_us()
        if ticks_diff(current_time,next_time)>=0:
            next_time = ticks_add(next_time,period)
            
            # Disable 
            if state == S1_SET:
                if clFlag.read() == True:
                    state = S2_ACTIVE
                else: 
                    yield None
            # Enable
            elif state == S2_ACTIVE:
                
                ang_vel = Velocity.read() # In units of degrees/s.
                eul_ang = Data.read() # In units of degrees.
                
                ClosedLoopControl_1.set_gain(Kp.read(), Ki.read(), Kd.read())
                ClosedLoopControl_2.set_gain(Kp.read(), Ki.read(), Kd.read())
                
                dt = ticks_diff(current_time, prev_time)/1000000
                
                Duty2.write(ClosedLoopControl_2.update(eul_ang[0], dt, ang_vel[0]))
                Duty1.write(ClosedLoopControl_1.update(eul_ang[1], dt, ang_vel[1]))

                if clFlag.read() == False:
                    state = S1_SET
                else:
                    yield None
            prev_time = current_time
                

            
        else:
            yield None