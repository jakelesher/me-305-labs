'''!@file               HW2page.py
    @brief              Creates the main documentation page for HW 2.
    @details            This file serves the sole purpose of generating html
                        documentation for HW 2.

    @page pagehw2       Homework 2 Deliverables

    @section sec_introhw2 Introduction
                        ME 305 HW 2 shows the ball balancer system modeling
                        that will be used in the final project. This page includes
                        my hand calulations of the equations of motion for this
                        system.
                        
    @section sec_fileshw2 Files in Lab 2
                        This homework was broken into 5 main steps, with each of
                        my attached pages of calculations pertaining to one step.
                        Below are the hand calculations for this homework:
                            
                        \htmlonly
                        <a href="https://ibb.co/wYsTmJF"><img src="https://i.ibb.co/stHhY5d/Page1.png" alt="Page1" border="0"></a>
                        <a href="https://ibb.co/SKcN4JK"><img src="https://i.ibb.co/XzDXrVz/Page2.png" alt="Page2" border="0"></a>
                        <a href="https://ibb.co/ZXnVMHB"><img src="https://i.ibb.co/YQz3LDc/Page3.png" alt="Page3" border="0"></a>
                        <a href="https://ibb.co/cc4dT4L"><img src="https://i.ibb.co/3ygHhgz/Page4.png" alt="Page4" border="0"></a>
                        <a href="https://ibb.co/WVVrPzf"><img src="https://i.ibb.co/mNNnXy4/Page5.png" alt="Page5" border="0"></a>
                        \endhtmlonly
    


    @author             Jake Lesher
    
    @copyright          License Info

    @date               February 13, 2022
'''