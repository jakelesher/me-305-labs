'''!
    @file       ClosedLoop.py
    
    @brief      Driver that sets the duty cycle of motor 1 to follow closed-loop 
                control.
    
    @details    This driver communicates mainly with taskController, which sends
                duty cycle values calculated using the error.
    
                The source code can be found at my repository under "Lab 4":
                https://bitbucket.org/jakelesher/me-305-labs
                
    @author     Jake Lesher
    @author     Daniel Xu
    @date       02/23/2022
    
'''
import pyb

class ClosedLoop:
    '''!
        @brief      
        @details    
                    
    '''
    def __init__(self):
        '''!@brief      A function that initializes the controller driver.
            @details    This fucntion sets the limits for closed-loop duty cycles.
        '''
        self.maxDuty = 120
        self.minDuty = -120
        self.int_error = 0
        
    def update(self,omega_meas,dt):
        '''!
            @brief      Updates the duty cycle under closed-loop control.
            @details    This function is in charge of actually using the gains
                        to assign a duty cycle to motor 1 to minimize error.
            @paramq     omega_meas is the current velocity of the motor.
            @return     Duty percentage of motor 1 is returned.
            
        '''
        
        self.omega_meas = omega_meas
        self.time_diff = dt
        self.error = self.omega_ref - self.omega_meas
        self.int_error += self.time_diff*self.error
        self.Duty = self.Kp* self.error + self.KI*self.int_error
        
        if self.Duty > self.maxDuty:
            self.Duty = self.maxDuty
        elif self.Duty < self.minDuty:
            self.Duty = self.minDuty
            
        return self.Duty
        
    def set_gain(self,Kp,KI,omega_ref):
        '''!@brief      Sets the gain and velocity reference values.
            @details    This function passed the user-selected values for gain
                        and Vref and passes them to the update() function.
            @param      Kp is the share of the user-requested proportional gain.
            @param      Ki is the share of the user-requested integral gain. 
            @param      omega_ref is the velocity requested by the user in
                        taskUser.
        '''
        self.Kp = Kp
        self.KI = KI
        self.omega_ref = omega_ref

    