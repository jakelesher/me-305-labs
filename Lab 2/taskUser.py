'''!
    @file       taskUser.py
    
    @brief      The task that will run the user interface.
    
    @details    This task takes in keyboard commands from the user to perform 
                certain tasks. This allows for the user to interact with the 
                Encoder task to determine instanenous data or to collect data 
                for a period of time. This is done by defining various states 
                that perform different tasks.
    
                The source code can be found at my repository under "Lab 2":
                https://bitbucket.org/jakelesher/me-305-labs
                
    @author     Jake Lesher
    @author     Daniel Xu
    @date       01/31/2022
'''

from time import ticks_us, ticks_diff, ticks_add, ticks_ms
from pyb import USB_VCP
import micropython, shares, array

# Defining the different states of taskUser.py
# Initialization State 
S0_INIT = micropython.const(0)
# Command Center State
S1_CMD = micropython.const(1)
# Zero the encoder State
S2_ZERO = micropython.const(2)
# Output Position State
S3_POSITION = micropython.const(3)
# Output Delta State
S4_DELTA = micropython.const(4)
# Collect Data State
S5_GET = micropython.const(5)
# Stop Data Collection State
S6_STOP = micropython.const(6)
# Output Data State
S7_DATA = micropython.const(7)

def printHelp():
    '''!@brief      This function outputs the GUI of encoder
        @details    The different commands of the user interface is outputted 
                    here.
            
    '''
    print("------------------------------")
    print("Welcome to the encoder wizard!")
    print("------------------------------")
    print("Press Z to zero the encoder.")
    print("Press P to print current position.")
    print("Press D to print current delta.")
    print("Press G to collect data for 30s.")
    print("Press S to stop data collection.")
    print("------------------------------")


def taskUserFcn (taskName, period, zFlag, Data, Delta):
    '''!@brief      This function serves as the main user interface.
        @details    This functions allows for the user to communicate with the 
                    backend using shared data and queues. It allows for the 
                    user to input different commands to perform different 
                    actions like zeroing the position of the encoder, determing 
                    the instantaneous position of the encoder, the change of 
                    position from the most immediate last position to the 
                    current position, and collect data.
        @param      taskName is the name associated the with the taskUser in 
                    main. 
        @param      period is the frequency of which the taskUser is to be run.
        @param      zFlag is the shared variable that communicates the 
                    taskEncoder to determine whether the encoder should be 
                    zeroed or not.
        @param      Data is the shared queue of positional data with 
                    taskEncoder.py.
        @param      Delta is the shared queue of the delta value with 
                    taskEncoder.py.
                    
    '''
    # Initialization State
    state = S0_INIT
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    ser = USB_VCP()    
    collect_data = False
    
    ##  @brief      The array, timeArray, is the collection of time where
    #               data is being collected.
    #   @details    The timeArray is predefined to collected 3001 data points,
    #               associated with 0-30s. This array is filled in updated time
    #               values when the data collection state is called.
    #  
    timeArray = array.array('h',3001*[0])
    
    ##  @brief      The array, positionArray, is the collection of position 
    #               when data is being collected.
    #   @details    The positionArray is predefined to collected 3001 data 
    #               points, associated with 0-30s. This array is filled in 
    #               updated position values when the data collection state is 
    #               called.
    #  
    positionArray = array.array('l', 3001*[0])
    
    while True:
        
        current_time = ticks_us()
        
        if Data.num_in() > 0 or Delta.num_in() > 0:
            
            ##  @brief      The positional data of the encoder
            #   @details    The variable position_data is the positional data 
            #               of the encoder, received from the taskEncoder. This 
            #               value is updated every loop.
            #  
            position_data = Data.get()
            
            ##  @brief      The delta data of the encoder
            #   @details    The variable delta_data is the change in position 
            #               from the most recent position to the current 
            #               position. This data is received from the 
            #               taskEncoder. This value is updated every loop.
            #  
            delta_data = Delta.get()
        else: 
            yield None
        
        if ticks_diff(current_time, next_time) >= 0:
            
            # Resetting the next_time so that it can apply to the next 
            # iteration of the while loop.
            next_time = ticks_add(next_time, period)
            
            # State 0  (Initialization) 
            if state == S0_INIT :
                printHelp()
                state = S1_CMD

            # State 1 (Waiting)
            elif state == S1_CMD:
                
                # Check VCP to see if there is a character waiting.
                # This if statement will primarily handle state transitions.
                if ser.any():
                    # Read one character and decode it into a string
                    charIn = ser.read(1).decode()
                    
                    if charIn in {'z', 'Z'}:
                        zFlag.write(True)
                        state = S2_ZERO # transition to state 2
                        
                    elif charIn in {'p', 'P'}:
                        state = S3_POSITION # transition to state 3
                        
                    elif charIn in {'d', 'D'}:
                        state = S4_DELTA # transition to state 4
                        
                    elif charIn in {'g', 'G'}:   
                        state = S5_GET # transition to state 5
                        
                    elif charIn in {'s', 'S'}:
                        state = S6_STOP # transition to state 6
                        
                    else:
                        print(f"You typed {charIn} from state 1")
                        print(f"at t={ticks_diff(current_time,start_time)/1e6}[s].")
            
            elif state == S2_ZERO:
                if zFlag.read() == False:
                    print("Zeroing encoder at current position.")

                    state = S1_CMD

            elif state == S3_POSITION:
                print("State 3: Print Position")
                print(f"The current position is {position_data}")
                state = S1_CMD

            elif state == S4_DELTA:
                print("State 4: Print Delta")
                print(f"Delta is currently {delta_data}")
                state = S1_CMD
                
            elif state == S5_GET:
                print("State 5: Collecting Data...")
                Num_data_collected = 0
                data_start_time = ticks_ms()
                collect_data = True
                state = S1_CMD

            elif state == S6_STOP:
                print("State 6: Stopping Data Collection")
                collect_data = False
                if Num_data_collected > 1:
                    state = S7_DATA
                else:
                    state = S1_CMD
                
            elif state == S7_DATA:
                print("State 7: Outputting Data:")
                for numItems in range(0,Num_data_collected):
                    print(f"{(timeArray[numItems]/1000):.2f},{positionArray[numItems]}")
                state = S1_CMD
                
            else:
                 raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
                 
            yield state
            
            # Data Collection for State 5
            if collect_data == True:
                data_current_time = ticks_ms()
                timeArray[Num_data_collected] = ticks_diff(data_current_time, data_start_time)
                positionArray[Num_data_collected] = position_data
                Num_data_collected += 1
            
                if Num_data_collected > 3000:
                    state = S7_DATA
                    collect_data = False
                else: 
                    yield None

            else:
                yield None


                
             
        else:
            yield None
                 
                 