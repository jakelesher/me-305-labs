'''!@file               mainpage.py
    @brief              Brief doc for mainpage.py
    @details            Detailed doc for mainpage.py 

    @mainpage           Mechatronics

    @section sec_intro  Mechatronics - ME 305
                        This portfolio provides documentation for the Python
                        files created in ME 305 by Jake Lesher and Daniel Xu.

    @section sec_docs   Documentation and Deliverables
                        Visit https://jakelesher.bitbucket.io/pages.html to
                        see the deliverables for each of the lab/HW assignments.
                        Lab 1: https://jakelesher.bitbucket.io/page1.html
                        Lab 2: https://jakelesher.bitbucket.io/page2.html
                        Lab 3: https://jakelesher.bitbucket.io/page3.html
                        Lab 4: https://jakelesher.bitbucket.io/page4.html
                        Lab 5: https://jakelesher.bitbucket.io/page5.html
                        Final Project: https://jakelesher.bitbucket.io/page6.html
                        
    @section sec_code   Source Code
                        All source code for these labs can be found in my 
                        repository at: https://bitbucket.org/jakelesher/me-305-labs

    @author             Jake Lesher
    @author             Daniel Xu
    
    @copyright          License Info

    @date               March 16, 2022
'''